#!/bin/sh

docker run --rm --detach --interactive --tty \
           --hostname demo-nginx --name demo-nginx \
           --network bridge --publish 80:80/tcp \
           --health-cmd "wget -s -q http://127.0.0.1/ && echo \"Up\" || echo \"Down\"" \
           --health-interval 5s --health-timeout 3s \
       library/nginx:stable-alpine
sleep 5s
docker logs demo-nginx
docker ps --filter "name=demo-nginx" --format "{{ .Status }}"
