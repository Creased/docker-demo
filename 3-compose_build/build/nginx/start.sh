#!/bin/sh

###
# nginx
#
nginx -g "daemon on;"

##
# logs
#
tail -F /var/log/nginx/access.log /var/log/nginx/error.log
